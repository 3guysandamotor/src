#include "mbed.h"
#include "TextLCD.h"
 
DigitalIn  din(D6);
TextLCD lcd(A0, A1, A2, A3, A4, A5); 
InterruptIn pulseInterrupt(D6);
Timer t;
 
//Variable declaration and initialization
int pulseCount = 0;
int num_of_rot = 0;
const float c = 0.081681408;
float distance = 0;
const float pi = 3.141592;
const float wheelRadius = 0.013; //Change to required value (in meters)
 
//Function is called upon detecting a rising edge on Pin C4 (See Interrupt below)
void highPulseDetected() {
    pulseCount++;
}
 
int main() {
      
    pulseInterrupt.rise(&highPulseDetected); //Interrupt for detecting rising edge (magnet present)
    
    
   while(1){ 
       num_of_rot = pulseCount/20;
       distance = c * num_of_rot;

    //printf("Number of Rotations = %d\n", num_of_rot);
    printf("Distance = %.2f\n", distance, "meters");
    
   lcd.printf("Dist = %.2f m \n\n", distance);
   lcd.printf("\n\n");
   
  }  
}
 