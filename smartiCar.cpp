#include "mbed.h"
#include "beep.h"
#include "ultrasonic.h"
#include "TextLCD.h"

//Servo
PwmOut servo ( A6 );

//Motor A 
PwmOut enA( D9 ); 
DigitalOut in1( D13 ); 
DigitalOut in2( A2 );

//Motor B
PwmOut enB( D9 );
DigitalOut in3( A3 ); 
DigitalOut in4( A7 );

//led turn signals
DigitalOut left_led(D0);
DigitalOut right_led(D3);
DigitalOut brake_led(D2);

//Speed/Distance Encoder
DigitalIn din(D11);
InterruptIn pulseInterrupt(D11);
Timer t;

//TextLCD
TextLCD lcd(A0, A1, D4, D5, D6, D7); // rs, e, d4-d7

//Beep
Beep buzzer(D12);

//Variable declaration and initialization
int pulseCount = 0;
int pulseCount1 = 0;
int timerS = 0;
int rps = 0;
int rpm = 0;
int num_of_rot = 0;
float kph = 0;
float distance = 0;
const float c = 0.081681408;
const float pi = 3.141592;
const float wheelRadius = 0.013; //Change to required value (in meters)

typedef enum
{   
    Slow_Left_Turn,
    Slow_Right_Turn,
    Go_Left_Turn,
    Go_Right_Turn,
    Full_Stop,
    Full_Stop_Left_Turn,
    Full_Stop_Right_Turn,
    Left_All_Off,
    Right_All_Off,
    All_Off,
    Reverse       
}Led_State;

Led_State Slow_Left_TurnHandler(void)
{   
    left_led = 1;
    right_led = 0;
    brake_led = 1;
    return Full_Stop_Left_Turn;
}
Led_State Full_Stop_Left_TurnHandler(void)
{
    left_led = 1;
    right_led = 0;
    brake_led = 1;
    return Go_Left_Turn;
}    
  
Led_State Go_Left_TurnHandler(void)
{
    left_led = 1;
    right_led = 0;
    brake_led = 0;
    return Left_All_Off;   
}
    
Led_State Slow_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    brake_led = 1;
    return Full_Stop_Right_Turn;   
}

Led_State Full_Stop_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    brake_led = 1;
    return Go_Right_Turn;
}  

Led_State Go_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    brake_led = 0;
    return Right_All_Off;
}

Led_State Full_StopHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 1;
    return Full_Stop;
}

Led_State All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return All_Off;
}

Led_State Left_All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return Slow_Left_Turn;
}

Led_State Right_All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return Slow_Right_Turn;
}

Led_State ReverseHandler(void)
{
    left_led = 1;
    right_led = 1;
    brake_led = 1;
    return All_Off;
}

void dist(int distance)
{
    lcd.printf("Dist:%i inches\n", distance);
    lcd.printf("\n");   
}

//Distance Sensor
ultrasonic mu(D8, D1, .1, 4, &dist); //Set the trigger pin to A4 and the echo pin to A5

void turnLeft();
void turnRight();
void backItUp();
void fullThrottle();
void chiiiill();

void fullThrottle()
{
    in1.write(0);
    in2.write(1);
    in3.write(0);
    in4.write(1);
    
    enA.write(1.0f);
}

void chiiiill()
{
    in1.write(0);
    in2.write(0);
    in3.write(0);
    in4.write(0);
    
    enA.write(0.0f);
}

void backItUp()
{
    in1.write(1);
    in2.write(0);
    in3.write(1);
    in4.write(0);
    
    enA.write(1.0f);
}

void turnLeft()
{
    in1.write(0);
    in2.write(1);
    in3.write(0);
    in4.write(0);
    
    enA.write(0.5f);
}

void turnRight()
{
    in1.write(0);
    in2.write(0);
    in3.write(0);
    in4.write(1);
    
    enA.write(0.5f);
}

//Function is called upon detecting a rising edge on Pin C4 (See Interrupt below)
void highPulseDetected() {
    pulseCount++;
    pulseCount1++;
}

//Speed Encoder and Print to LCD Display
void SpeedEncoder()
{
    if(timerS >= 1000){ //Once a second has passed
        num_of_rot = pulseCount/20;
        rps = pulseCount; //Read pulses counted after 1 second.
        rpm = (pulseCount/20) * 60; //Convert revolutions per second in revolutions per minute
        kph = 2 * pi * wheelRadius * rpm * 0.06; //Convert revolutions per minute in KM/H
        pulseCount = 0; //Reset pulseCount for a new speed update
        t.reset(); //Reset timer to get a new speed update
        }
        lcd.printf("rpm = %d\n", rpm);
        lcd.printf("\n\n");
}

//Distance Encoder
void DistanceEncoder()
{
    num_of_rot = pulseCount1/20;
    distance = c * num_of_rot;
    lcd.printf("Dist = %.2f M\n", distance);
    lcd.printf("\n\n");   
}

int main()
{
    Led_State NextState = All_OffHandler();
    //Interrupt for detecting rising edge (magnet present)
    pulseInterrupt.rise(&highPulseDetected); 
    
    //servo pwm
    servo.period_ms(20);
    servo.pulsewidth(0.0012); // 90-degrees
    wait(2);
    
    //starts getting distances
    mu.startUpdates();
    
    //start the engines
    wait(4);
    fullThrottle();
    
    while(true)
    {
        NextState = All_OffHandler();
        t.start();
        timerS = t.read() * 1000; //Read timer value and convert to milliseconds.
        int dist = mu.getCurrentDistance();
        
        if(dist > 6){
        DistanceEncoder();
        SpeedEncoder();
        }
        
        if(dist <= 6)
        {   
            lcd.cls();
            mu.checkDistance();
            buzzer.beep(500, 0.5);
            //Brake LED ON
            NextState = Full_StopHandler();
            //Left Turn LED ON
            NextState = Full_Stop_Left_TurnHandler();
            //check left
            servo.pulsewidth(0.002); // 180-degrees
            wait(2);
            dist = 0;
            dist = mu.getCurrentDistance();
            if(dist <= 6)
            {
                //Right Turn LED ON
                NextState = Full_Stop_Right_TurnHandler();
                //check right
                servo.pulsewidth(0.0001); // 0-degrees
                wait(2);
                dist = 0;
                dist = mu.getCurrentDistance();
                if(dist <= 6)
                {
                    //Reverse LEDS ON
                    NextState = ReverseHandler();
                    servo.pulsewidth(0.0012); // 90-degrees
                    wait(2);
                    backItUp();
                    wait(2);
                    //Right Turn LED ON
                    NextState = Go_Right_TurnHandler();
                    turnRight();
                    wait(0.75f);
                }
                else
                {
                    //Right LED ON
                    NextState = Go_Right_TurnHandler();
                    servo.pulsewidth(0.0012); // 90-degrees
                    wait(2);
                    //go right
                    turnRight();
                    wait(0.75f);
                }
                
            }
            else
            {
                //Left LED ON
                NextState = Go_Left_TurnHandler();
                servo.pulsewidth(0.0012); // 90-degrees
                wait(2);
                //go left
                turnLeft();
                wait(0.75f);
            }
        }
               
        fullThrottle();
            
    }
    
}

/*
ALBERTO ANDRADE
I created the functions for the DC motor for it to go forward or go backward using a PWM pin and the HBridge from the STM32 Nucleo board.
The functions are called: fullThrottle() and backItUp()

I also made the code for the DC motor to turn left or turn right. This was done by turning off the motor from one wheel and turning on
the motor from the other wheel for 0.75 seconds. Then turn both motors on to go straight again.
The functions are called: turnLeft() and turnRight()

I added the servo rotations also. They are set up so that when the ultrasonic sensor sees something it turn either 0-90-or 180-degrees.
The calls are the initial servo.period(20) and then the servo.pulsewidth(0.00001) for 0-degrees, servo.pulsewidth(0.0012) for 90-degrees, 
and servo.pulsewidth(0.002).

*/

/*
JUSTIN LIEU
I created the functions for the distance sensor to print the distance to print out on the LCD Screen.
The function is called: void dist(int distance)

I implemented the calls which makes the distance sensor start checking the distance updating tthe distance, and capturing the distance.
The calls are: mu.startUpdates(), mu.checkDistance(), and mu.getCurrentDistance.

I also implemented the piezo buzzer part of the code that made the buzzer beep when the sensor recieves a distance of 
6 inches or less.
The call is buzzer.beep(500, 0.5);

*/

/*
Kelly Lopez
I created the function of the speed sensor to read the inputs from the LM393 chip connected to the speed encoder of the SmartiCar. 
The speed of the car is set to rpm and is outputted on the LCD screen. The function is called void SpeedEncoder()

I also created the function called void DistanceEncoder() to calculate the distance the robot smart car is traveling and will keep updating 
the speed until the car has stopped or no longer working. This is also using the same circuit design as the speed sensor and created a seperate
variable to store the number of pulses being counted by the system to avoid errors with the rpm readings. The measurements are also displayed 
on the LCD screen and the unit of measurement I chose for this is coded in meters.

Lastly, I also created a variety of states for the LEDs attached to the car which mimic as close as possible a real world vehicle. It 
contains states such as left turn, right turn, full stop, foward state, reverse state, etc. These LEDs will turn on before the car turns
so as to mimic an actual person who is slowing down to and turning on their signal to indicate to the other people that they are about
to turn and proceed on their path. Rather than making a simple code to turn on the LEDs, I attempted to create this in a form of a finite state
machine to the best of my abilities by the use of handlers and call variables to these states.
*/

/*
Andrew Hall
I created the function of turning the headlights when the light is measured to be below the threshhold through the phototransister. 
It was powered throughg the STM32 chip and placed in the front of the car to replicate actual headlights.
I used meas = photocell.read();
       where,  meas = meas *4000;

I also created an Analog to Digital Converter to measure a reference voltage through a resistor that would ultimately give us a reading of the current draw.
***I tried to implement this with our DC motor and unfortunately it because a complex task and time ran out before we could inegrate it fully. 
I used AnalogIn analog_value(A1) and DigitalOut led( A0 ) to acheive this.

Lastly I created a song using the Peizo Speaker using varying frequencies and pauses that would come together to sound something like the Tetris Song
The call would be buzzer.beep(#,#);

Phototransistor Code:

#include "mbed.h"
DigitalIn photocell(D7);
DigitalOut myled(D8);
DigitalOut myled2(D9);

int main() {
    float meas; 
    while(1) {
        meas = photocell.read();
        meas = meas *4000;
        if(meas < 10)
        {
            myled = 1;
	myled2 = 1;
        }
        else
        {
            myled = 0;
Myled2 = 0;
        }
        //myled = photocell; // LED is ON
        //wait(0.1); // 200 ms
    }
}


\\ Analog to digital (Voltage measurement for Current draw):


#include "mbed.h"

AnalogIn analog_value(A1);

DigitalOut led( A0 );

int main()
{
    float meas_r;
    float meas_v;

    while(1) {

        meas_r = analog_value.read(); // Read the analog input value (value from 0.0 to 1.0 = full ADC conversion range)
        meas_v = meas_r *3.8; // Converts value in the 0V-3.8V range
        
        // LED is ON when the value is above 2V
        if (meas_v >1.5) {
            led = 1; // LED ON
        } else {
            led = 0; // LED OFF
        }
        printf ("The voltage across the led is: %0.2f \n", meas_v);
        //wait(1); // 200 millisecond
    }
}






\\ Car Song (Buzzer Code)

{

  buzzer.beep(1568,0.5);
    wait(0.25);
buzzer.beep(1397,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1175,0.5);
wait(0.25);
buzzer.beep(1046,0.5);
wait(0.25);
buzzer.beep(1046,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
wait(0.25);
buzzer.beep(1175,0.75);
wait(0.25);
buzzer.beep(1245,0.75);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
buzzer.beep(1568,0.5);
    wait(0.25);
buzzer.beep(1397,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1175,0.5);
wait(0.25);
buzzer.beep(1046,0.5);
wait(0.25);
buzzer.beep(1046,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
wait(0.25);
buzzer.beep(1175,0.75);
wait(0.25);
buzzer.beep(1245,0.75);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
buzzer.beep(1568,0.5);
    wait(0.25);
buzzer.beep(1397,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1175,0.5);
wait(0.25);
buzzer.beep(1046,0.5);
wait(0.25);
buzzer.beep(1046,0.25);
wait(0.25);
buzzer.beep(1245,0.25);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
wait(0.25);
buzzer.beep(1175,0.75);
wait(0.25);
buzzer.beep(1245,0.75);
wait(0.25);
buzzer.beep(1397,0.5);
wait(0.25);
buzzer.beep(1568,0.5);
}


