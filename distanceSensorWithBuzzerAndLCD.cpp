#include "mbed.h"
#include "ultrasonic.h"
#include "beep.h"
#include "TextLCD.h"
Beep detect(A2);
TextLCD lcd(A0, A1, D4, D5,D6,D7,TextLCD::LCD20x4); // rs, e, d4-d7

 void dist(int distance)
{
    //put code here to happen when the distance is changed
    printf("Distance changed to %d inches\r\n", distance); 
    lcd.printf("Dist:%i inches   \n",distance);
    lcd.printf("\n");
    
}

ultrasonic mu(D8, D9, .1, 4, &dist);    //Set the trigger pin to D8 and the echo pin to D9
                                        //have updates every .1 seconds and a timeout after 1
                                        //second, and call dist when the distance changes


int main()
{
    mu.startUpdates();//start mesuring the distance
    while(1)
    {
        //Do something else here
        mu.checkDistance();     //call checkDistance() as much as possible, as this is where
                                //the class checks if dist needs to be called.
        int dist = mu.getCurrentDistance();
        
        if (dist <= 1) {

            detect.beep(1000,1);
        } else {
            detect.nobeep();   
        }

    }
}
