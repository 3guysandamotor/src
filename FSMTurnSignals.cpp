#include "mbed.h"
#include "ultrasonic.h"
#include "TextLCD.h"
DigitalOut left_led(D10);
DigitalOut right_led(D11);
DigitalOut brake_led(D12);
TextLCD lcd(A0, A1, A2, A3, A4, A5);

typedef enum
{   
    Slow_Left_Turn,
    Slow_Right_Turn,
    Go_Left_Turn,
    Go_Right_Turn,
    Full_Stop,
    Full_Stop_Left_Turn,
    Full_Stop_Right_Turn,
    Left_All_Off,
    Right_All_Off,
    All_Off       
}Led_State;

Led_State Slow_Left_TurnHandler(void)
{   
    left_led = 1;
    wait(0.2);
    left_led = 0;
    wait(0.2);
    right_led = 0;
    brake_led = 1;
    return Full_Stop_Left_Turn;
}
Led_State Full_Stop_Left_TurnHandler(void)
{
    left_led = 1;
    wait(0.2);
    left_led = 0;
    wait(0.2);
    right_led = 0;
    brake_led = 1;
    return Go_Left_Turn;
}    
  
Led_State Go_Left_TurnHandler(void)
{
    left_led = 1;
    wait(0.2);
    left_led = 0;
    wait(0.2);
    right_led = 0;
    brake_led = 0;
    return Left_All_Off;   
}
    
Led_State Slow_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    wait(0.2);
    right_led = 0;
    wait(0.2);
    brake_led = 1;
    return Full_Stop_Right_Turn;   
}

Led_State Full_Stop_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    wait(0.2);
    right_led = 0;
    wait(0.2);
    brake_led = 1;
    return Go_Right_Turn;
}  

Led_State Go_Right_TurnHandler(void)
{
    left_led = 0;
    right_led = 1;
    wait(0.2);
    right_led = 0;
    wait(0.2);
    brake_led = 0;
    return Right_All_Off;
}

Led_State Full_StopHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 1;
    return All_Off;
}

Led_State All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return Full_Stop;
}

Led_State Left_All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return Slow_Left_Turn;
}

Led_State Right_All_OffHandler(void)
{
    left_led = 0;
    right_led = 0;
    brake_led = 0;
    return Slow_Right_Turn;
}
   
 void dist(int distance)
{
    //put code here to happen when the distance is changed
    printf("Distance changed to %d inches\r\n", distance);
}



ultrasonic mu(D8, D9, .1, 4, &dist);    //Set the trigger pin to D8 and the echo pin to D9
                                        //have updates every .1 seconds and a timeout after 1
                                        //second, and call dist when the distance changes


int main()
{
   
    Led_State NextState;
    mu.startUpdates();//start mesuring the distance
    int r;
    r = rand()%2+1;
    printf("%d\n",r);
    if(r == 1){
        NextState = Slow_Left_Turn;}
    else{
        NextState = Slow_Right_Turn;}
   
    while(1)
    {   
        mu.checkDistance();
        int dist = mu.getCurrentDistance();
        //Do something else here
        mu.checkDistance();     //call checkDistance() as much as possible, as this is where
                                //the class checks if dist needs to be called.

        if(dist < 6)
        {
            switch(NextState)
            {
                case Slow_Left_Turn:
                NextState = Slow_Left_TurnHandler();
                lcd.printf("Slowing down making Left Turn");
                wait(2.0);
                break;
                case Full_Stop_Left_Turn:
                NextState = Full_Stop_Left_TurnHandler();
                lcd.printf("Full Stop Left Turn");
                wait(2.0);
                break;
                case Go_Left_Turn:
                NextState = Go_Left_TurnHandler();
                lcd.printf("Driving and going left");
                wait(2.0);
                break;
                case Slow_Right_Turn:
                NextState = Slow_Right_TurnHandler();
                lcd.printf("Slowing down making right turn");
                wait(2.0);
                break;
                case Full_Stop_Right_Turn:
                NextState = Full_Stop_Right_TurnHandler();
                lcd.printf("Full Stop Left Turn");
                wait(2.0);
                break;
                case Go_Right_Turn:
                NextState = Go_Right_TurnHandler();
                lcd.printf("Driving and going right");
                wait(2.0);
                break;
                case Full_Stop:
                NextState = Full_StopHandler();
                break;
                default:
                NextState = All_OffHandler();
                break;
            }
        }
        
    }
}
