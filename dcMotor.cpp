#include "mbed.h"

//Motor A
PwmOut enA( D9 );
DigitalOut in1 ( A0 );
DigitalOut in2 ( A1 );

int main()
{
    while(true)
    {
        //set motor A to forward position
        in1.write(0);
        in2.write(1);
        
        //accelerate
        for(float i = 0.0; i < 1.0 ; i += 0.01)
        {
            enA.write(i);
            wait_ms(20);
        }    
        
        //slow down
        for(float i = 1.0; i > 0.0; i -= 0.01)
        {
            enA.write(i);
            wait_ms(20);
        }
        
        //brakes
        in1.write(0);
        in2.write(0);
        
        wait(1);
        
        //set motor A to reverse position
        in1.write(1);
        in2.write(0);
        
        //accelerate
        for(float i = 0.0; i < 1.0 ; i += 0.01)
        {
            enA.write(i);
            wait_ms(20);
        }    
        
        //slow down
        for(float i = 1.0; i > 0.0; i -= 0.01)
        {
            enA.write(i);
            wait_ms(20);
        }
        
        //brakes
        in1.write(0);
        in2.write(0);
        
        wait(1);
        
    }
}