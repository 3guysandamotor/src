#include "mbed.h"
DigitalIn photocell(D7);
DigitalOut myled(D8);
DigitalOut myled2(D9);

int main() {
    float meas; 
    while(1) {
        meas = photocell.read();
        meas = meas *4000;
        printf("Brightness level: %.2f",meas);
        if(meas < 10)
        {
            myled = 1;
            myled2 = 1;
        }
        else
        {
            myled = 0;
            myled2 = 0;
        }
        //myled = photocell; // LED is ON
        //wait(0.1); // 200 ms
    }
}

