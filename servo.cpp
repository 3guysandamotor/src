#include "mbed.h"

PwmOut servo( A6 );

int main()
{
    //testing the different angles
    servo.period_ms(20);
    servo.pulsewidth(0.0001); // 0-degrees
    wait(1);
    servo.pulsewidth(0.0012); // 90-degrees
    wait(1);
    servo.pulsewidth(0.002); // 180-degrees
    wait(1);
    
    //reset to forward position
    servo.pulsewidth(0.0012); // 90-degrees
    wait(1);
    
    for(int i=0; i<3; i++)
    {
        //turn left
        servo.pulsewidth(0.0012); // 90-degrees
        wait(1);
        servo.pulsewidth(0.002); // 180-degrees
        wait(1);
        servo.pulsewidth(0.0012); // 90-degrees
        wait(3);
        
        //turn right
        servo.pulsewidth(0.0012); // 90-degrees
        wait(1);
        servo.pulsewidth(0.00001); // 0-degrees
        wait(1);
        servo.pulsewidth(0.0012); // 90-degrees
        wait(3);
        
    }
}