Analog to digital (Voltage measurement for Current draw):


#include "mbed.h"

AnalogIn analog_value(A1);

DigitalOut led( A0 );

int main()
{
    float meas_r;
    float meas_v;

    while(1) {

        meas_r = analog_value.read(); // Read the analog input value (value from 0.0 to 1.0 = full ADC conversion range)
        meas_v = meas_r *3.8; // Converts value in the 0V-3.8V range
        
        // LED is ON when the value is above 2V
        if (meas_v >1.5) {
            led = 1; // LED ON
        } else {
            led = 0; // LED OFF
        }
        printf ("The voltage across the led is: %0.2f \n", meas_v);
        //wait(1); // 200 millisecond
    }
}

